# README

# Zamek elektroniczny z wielopoziomową kontrolą dostępu 

Zamek posiada trzy poziomy dostępu:
* 4 cyfrowy kod wpisywany z klawiatury membranowej 4x4
* 4 cyfrowy kod wpisywany za pomocą czterech przycisków analogowych
* dostęp poprzez użycie karty RFID

Urządzenie pozwala na przejście do stanu serwisowego w celu zmiany kodów dostępu, oraz na uzbrojenie poprzez aplikację mobilną RemoteXY za pomocą Wi-Fi. 
Projekt zbudowany w oparciu o środowisko Arduino.

## Wersja ##
1.0 23/05/20

## Dokumentacja online ##

Link do interaktywnej wersji dokumentacji:
https://hertwing.github.io/dokumentacja_projekt_inzynierski/index.html

## Prezentacja urządzenia ##

Film z prezentacją znajduje się pod linkiem:
https://drive.google.com/file/d/1ib5GVDQ1nmPHTfB74Y7UWHjOnHJj8P1z/view?usp=sharing

## Uruchamianie ##

* Po pobraniu plików z repozytorium należy otworzyć plik zamek.ino w Arduino IDE.
* W menadżerze bibliotek należy znaleźć bibliotekę RemoteXY i zainstalować ją do swojego IDE.
* Przed pierwszym uruchomieniem należy edytować tablicę globalną nuidCODE wprowadzając numery swojej karty RFID.
* Następnie zweryfikować projekt i wgrać na płytkę.
* Przy pierwszym uruchomieniu urządzenie poprosi użytkownika o wprowadzenie nowych kodów dostępu, należy postępować zgodnie z instrukcjami wyświetlacza LCD.
* W celu połączenia z siecią Wi-Fi należy skonfigurować pola w pliku pre_wifi.h #define REMOTEXY_WIFI_SSID zawierające nazwę sieci, oraz #define REMOTEXY_WIFI_PASSWORD zawierające hasło dostępu.

## Znane problemy ##
Podczas weryfikacji programu może wystąpic problem z rozpoznaniem bibliotek EEPORM.h i SPI.h.
W takim przypadku należy usunąć #include tych plików nagłówkowych i dodać je jeszcze raz ręcznie poprzez Arduino IDE.
