/*! \mainpage Zamek  elektroniczny  z  wielopoziomową  kontrolą  dostępu  (dziedzina:  systemy  wbudowane,  mikro-kontrolery, Arduino).
 *
 * \section intro_sec Informacje ogólne
 * 
 * \subsection purpose Cel projektu
 * Stworzenie elektronicznego zamka sterowanego za pomocą kontrolera Arduino przy pomocy trzech poziomów weryfikacji dostępu.
 *
 * \subsection repo Repozytorium z plikami projektu
 * https://bitbucket.org/henczelpiotr/projekt_inzynierski/src/master/
 * 
 * \subsection authors Autorzy 
 * Piotr Henczel\n 
 * Krzysztof Kaczmarski
 * 
 * \subsection version Wersja
 * 1.0 23/05/20
 * 
 * \section install_sec Uruchamianie
 *
 * \subsection step1 1: Otworzenie projektu w arduino IDE
 * 
 * Po pobraniu plików z repozytorium należy otworzyć plik zamek.ino w Arduino IDE.\n
 * W menadżerze bibliotek nalezy znaleźć bibliotekę RemoteXY i zainstalować ją do swojego IDE.\n
 * Przed pierwszym uruchomieniem należy edytować tablicę globalną nuidCODE wprowadzając numery swojej karty RFID. \n
 * Następnie zweryfikować projekt i wgrać na płytkę.
 * 
 * \subsection step2 2: Pierwsze uruchomienie
 * Przy pierwszym uruchomieniu urządzenie poprosi użytkownika o wprowadzenie nowych kodów dostępu, należy postępować zgodnie z instrukcjami wyświetlacza LCD.
 * 
 * \section known_issues Znane problemy
 * Podczas weryfikacji programu może wystąpic problem z rozpoznaniem bibliotek EEPORM.h i SPI.h.\n
 * W takim przypadku należy usunąć #include tych plików nagłówkowych i dodać je jeszcze raz ręcznie poprzez Arduino IDE.
 *
 * \section special_thanks Podziękowania
 * 
 * Projekt wykorzystuje między innymi zmodyfikowaną bibliotekę MFRC522, której oryginał znajduje się w repozytorium:\n 
 * https://github.com/miguelbalboa/rfid
 * 
 * Za bibliotekę dziękujęmy jej twórcy COOQROBOT.
 * 
 * Kolejną niestandardową biblioteką użytą w projekcie jest ShiftedLCD, której kod źródłowy znajduje się pod tym adresem:\n
 * https://github.com/omersiar/ShiftedLCD
 * 
 * Za tę bibliotekę pragniemy podziękować Hansowi-Christophowi Steinerowi
 */

#include <EEPROM.h>
#include <SPI.h>

// Zmodyfikowana bilbioteka MFRC522 z możliwością obsługi ekspandera MCP23017.
#include "MCP_MFRC522.h" 
// Plik nagłówkowy przetrzymujący dane wygenerowane za pomocą strony https://remotexy.com/, umożliwiające połączenie z modułem Wi-Fi.
#include "pre_wifi.h"  
// Plik nagłówkowy biblioteki ShiftedLCD umożliwiającej działanie wyświetlacza LCD z rejestrem przesuwnym SN74HC595.  
#include "ShiftedLCD.h"  

/// Pin MCP dla MicroServo.
#define MOTOR_IN 5

#define LED_1       8
#define LED_2       9
#define LED_3      10
#define LED_RED    11
#define LED_YELLOW 12

/// Pin MCP dla SPI czytnika kart.
#define RFID_SS_PIN 7 
/// Pin MCP dla resetu czytnika kart.
#define RFID_RST_PIN 6 

#define BUTTON_PIN_1 9
#define BUTTON_PIN_2 8
#define BUTTON_PIN_3 7
#define BUTTON_PIN_4 6

/// Obiekt ekspandera pindów MCP23017.
Adafruit_MCP23017 mcp; 

/// Obiekt dla modułu czytnika kart RFID.
MFRC522 rfid(RFID_SS_PIN, RFID_RST_PIN, &mcp); 

/// Tablica globalna będąca kodem karty RFID.
byte nuidCODE[4] = {249, 214, 82, 89}; 

/// Obiekt wyświetlacza LCD z komunikacją na pinie 10.
LiquidCrystal lcd(10); 

/// Zmienna globalna będąca iteratorem dla tablicy kodów 4x4 i przycisków analogowych.
uint8_t iterator_kodu = 0; 

/// Zmienna globalna przechowująca ciąg znaków reprezentujących kod klawiatury.
char kod[5] = "1234"; 

/// Zmienna globalna przechowująca ciąg znaków reprezentujących wpisany przez użytkownika kod klawiatury.
char wpisany_kod[5] = "####"; 

/// Zmienna globalna przechowująca ciąg znaków reprezentujących kod przycisków analogowych.
char kolejnosc_przyciskow[5] = "4321"; 

/// Zmienna globalna przechowująca ciąg znaków reprezentujących wpisany przez użytkownika kod przycisków.
char wpisane_przyciski[5] = "####"; 

/// Tablica globalna przechowująca kod klawiatury 4x4 w pamieci EEPROM.
byte ep_keypad[4];
/// Tablica globalna przechowująca kod przycisków analogowych w pamieci EEPROM.
byte ep_buttons[4];

/// Zmienna globalna do odczyania stanu zamka z pamięci EEPROM po uruchomieniu urządzenia.
bool ep_otwarty = 0; 

/// Zmienna globalna do odczytania pamięci EEPROM po uruchomieniu urządzenia.
bool read_eeprom = true; 

/// Zmienna globalna dla przycisku podłączonego do pinu analogowego A0.
uint16_t przycisk_A0;
/// Zmienna globalna dla przycisku podłączonego do pinu analogowego A1.
uint16_t przycisk_A1;
/// Zmienna globalna dla przycisku podłączonego do pinu analogowego A2.
uint16_t przycisk_A2;
/// Zmienna globalna dla przycisku podłączonego do pinu analogowego A3.
uint16_t przycisk_A3;

/// Tablica globalna przechowująca kod wyświetlany na LCD.
char kod_lcd[5] = "    "; 

/// Zmienna globalna aktualnego poziomu zabezpieczeń.
uint8_t poziom_zabezpieczenia = 1; 

/// Zmienna globalna przechowująca pozostałą ilość prób.
byte ilosc_prob = 3; 

/** Funkcja ustawiająca wartości początkowe programu.
 *  
 *  Zanim uruchomiona zostanie pętla właściwa, ustawiane są wartości początkowe programu, takie jak:
 *  Piny dla modułu MCP, reset kolorów diod LED, wczytywanie ostatniego stanu zamka z pamięci EEPROM,
 *  a następnie ustawienie odpowiedniego poziomu zabezpieczenia dla odczytanej wartości.
 *  Następuje również inicjalizacja modułu mcp, SPI, RFID, lcd oraz moduł RemoteXY.  
 */
void setup()
{
  mcp.begin();
  SPI.begin();
  rfid.PCD_Init();
  RemoteXY_Init();
 
  // Ustawianie pinow MCP
  mcp.pinMode(LED_1,      OUTPUT);
  mcp.pinMode(LED_2,      OUTPUT);
  mcp.pinMode(LED_3,      OUTPUT);
  mcp.pinMode(LED_RED,    OUTPUT);
  mcp.pinMode(LED_YELLOW, OUTPUT);

  mcp.pinMode(MOTOR_IN, OUTPUT);

  pinMode(BUTTON_PIN_1, INPUT);
  pinMode(BUTTON_PIN_2, INPUT);
  pinMode(BUTTON_PIN_3, INPUT);
  pinMode(BUTTON_PIN_4, INPUT);
  
  // Resetowanie LED
  reset_led();

  // Sprawdzenie w jakiej pozycji znajdował się zamek przed uruchomieniem urządzenia aby nadać odpowiedni kod zabezpieczeń.
  ep_otwarty = EEPROM.read(9);
  if(ep_otwarty==1 || ep_otwarty==255)
  {
    poziom_zabezpieczenia = 4;
  } else if(ep_otwarty==0) {
    poziom_zabezpieczenia = 1;
  }

  // Inicjalizacja LCD.
  lcd.begin(16,2);
}

/** Funkcja pobierająca przycisk z klawiatury 4x4
 * 
 *  Funkcja odczytuje wartości z funkcji analogRead() dla opowiedniego pinu analogowego,  
 *  następnie zwraca odpowiednią wartość char dla odczytanej wartości.
*/
char read_key()
{
  if(analogRead(A0)>150 && analogRead(A0)<220)
  {
    while(analogRead(A0)>150 && analogRead(A0)<220){delay(200);}
    return '1';
  }
  else if(analogRead(A0)>300 && analogRead(A0)<400)
  {
    while(analogRead(A0)>300 && analogRead(A0)<400){delay(200);}
    return '2';
  }
  else if(analogRead(A0)>550 && analogRead(A0)<700)
  {
    while(analogRead(A0)>550 && analogRead(A0)<700){
      delay(200);
    }
    return '3';
  }
  else if(analogRead(A0)>800)
  {
    while(analogRead(A0)>800){}
    return 'A';
  }
  else if(analogRead(A1)>150 && analogRead(A1)<220)
  {
    while(analogRead(A1)>150 && analogRead(A1)<220){delay(200);}
    return '4';
  }
  else if(analogRead(A1)>300 && analogRead(A1)<400)
  {
    while(analogRead(A1)>300 && analogRead(A1)<400){delay(200);}
    return '5';
  }
  else if(analogRead(A1)>550 && analogRead(A1)<700)
  {
    while(analogRead(A1)>550 && analogRead(A1)<700){delay(200);}
    return '6';
  }
  else if(analogRead(A1)>800)
  {
    while(analogRead(A1)>800){delay(200);}
    return 'B';
  }
  else if(analogRead(A2)>150 && analogRead(A2)<220)
  {
    while(analogRead(A2)>150 && analogRead(A2)<220){delay(200);}
    return '7';
  }
  else if(analogRead(A2)>300 && analogRead(A2)<400)
  {
    while(analogRead(A2)>300 && analogRead(A2)<400){delay(200);}
    return '8';
  }
  else if(analogRead(A2)>550 && analogRead(A2)<700)
  {
    while(analogRead(A2)>550 && analogRead(A2)<700){delay(200);}
    return '9';
  }
  else if(analogRead(A2)>800)
  {
    while(analogRead(A2)>800){delay(200);}
    return 'C';
  }
  else if(analogRead(A3)>150 && analogRead(A3)<220)
  {
    while(analogRead(A3)>150 && analogRead(A3)<220){delay(200);}
    return '*';
  }
  else if(analogRead(A3)>300 && analogRead(A3)<400)
  {
    while(analogRead(A3)>300 && analogRead(A3)<400){delay(200);}
    return '0';
  }
  else if(analogRead(A3)>550 && analogRead(A3)<700)
  {
    while(analogRead(A3)>550 && analogRead(A3)<700){delay(200);}
    return '#';
  }
  else if(analogRead(A3)>800)
  {
    while(analogRead(A3)>800){delay(200);}
    return 'D';
  }
  else return 'E';
}

/** Funkcja odczytywująca wciśnięty przycisk klawiatury analogowej.
 *
 *  Funkcja sprawdza czy dla danej wartości na pinie cyfrowym wystąpiła wartość HIGH
 *  i zwraca odpowiedni znak odpowiadający tej wartości.
 */
char read_button()
{
  while(true)
  {
    przycisk_A0 = digitalRead(BUTTON_PIN_1);
    przycisk_A1 = digitalRead(BUTTON_PIN_2);
    przycisk_A2 = digitalRead(BUTTON_PIN_3);
    przycisk_A3 = digitalRead(BUTTON_PIN_4);
    if(przycisk_A0==HIGH)
    {
      while(przycisk_A0==HIGH)
      {
        przycisk_A0 = digitalRead(BUTTON_PIN_1);
      }
      return '1';
    } else if(przycisk_A1==HIGH) {
      while(przycisk_A1==HIGH)
      {
        przycisk_A1 = digitalRead(BUTTON_PIN_2);
      }
      return '2';
    } else if(przycisk_A2==HIGH) {
      while(przycisk_A2==HIGH)
      {
        przycisk_A2 = digitalRead(BUTTON_PIN_3);
      }
      return '3';
    } else if(przycisk_A3==HIGH) {
      while(przycisk_A3==HIGH)
      {
        przycisk_A3 = digitalRead(BUTTON_PIN_4);
      }
      return '4';
    }
  }
}

/** Funkcja pozwalająca wporwadzić kody po raz pierwszy do systemu.
 *
 *  Jeśli wartości pamięci EEPROM znajdujące się w komórkach od 0 do 8 nie były nigdy zapisane hasłem dostępowym
 *  funkcja pozwala wpisać te wartości po raz pierwszy i zapisać je w pamięci EEPROM.  
 */
void wprowadz_kody()
{
  lcd.clear();
  wypisz_na_lcd(0, 0, "NOWY KOD 1");
  delay(1500);
  lcd.clear();
  wypisz_na_lcd(0, 0, "PODAJ NOWY KOD 1");
  reset_danych();
  char key;
  while(true)
  {
    key = read_key();
    if(key != 'E' && key != 'C')
    {
      //mechanizm wpisywania kodu
      wpisany_kod[iterator_kodu] = key;
      kod_lcd[iterator_kodu] = '*';
      iterator_kodu=iterator_kodu+1;
      lcd.clear();
      wypisz_na_lcd(0, 0, "PODAJ NOWY KOD 1");
      wypisz_na_lcd(0, 1, kod_lcd);
      lcd.setCursor(0,1);
      //sprawdzanie kodu
      if(iterator_kodu == 4)
      {
        strncpy(kod, wpisany_kod, sizeof(kod));
        for(byte i=0; i<4; i++)
        {
          EEPROM.write(i, kod[i]-48); // Wartość 48 z tablicy ASCII dla cyfr.
        }
        lcd.clear();
        wypisz_na_lcd(0, 0, "KOD 1 ZAPISANY.");
        delay(1500);
        break;
      }
    }
  }
  lcd.clear();
  wypisz_na_lcd(0, 0, "NOWY KOD 2");
  delay(1500);
  lcd.clear();
  wypisz_na_lcd(0, 0, "PODAJ NOWY KOD 2");
  reset_danych();
  while(true)
  {
    wpisane_przyciski[iterator_kodu]=read_button();
    kod_lcd[iterator_kodu] = '*';
    iterator_kodu=iterator_kodu+1;
    lcd.clear();
    wypisz_na_lcd(0, 0, "PODAJ NOWY KOD 2");
    wypisz_na_lcd(0, 1, kod_lcd);
    
    if(iterator_kodu==4)
    {
      strncpy(kolejnosc_przyciskow, wpisane_przyciski, sizeof(kolejnosc_przyciskow));
      for(byte i=0; i<4; i++)
        {
          EEPROM.write(i+4, kolejnosc_przyciskow[i]-48); // Wartość 48 z tablicy ASCII dla cyfr.
        }
      lcd.clear();
      wypisz_na_lcd(0, 0, "KOD 2 ZAPISANY.");
      delay(1500);
      break;
    }
  }
  reset_zamka();
}

/** Funkcja zmieniająca pozycję zamka symulowanego za pomocą MicroServo.
 *
 *  Zadaniem funkcji jest zmiana pozycji łopatki silnika MicroServo z otwartej na zamkniętą, lub z zamkniętej na otwartą.
 *  Funkcja korzysta z pętli sterującej silnikiem za pomocą wysyłania naprzemiennie sygnałów HIGH i LOW na odpowiedni pin ekspandera MCP.
 *  @param x odpowiada pozycji zamka, 0 - otwarty, 240 - zamknięty
 */
void pozycja_zamka(int x)
{
  int del=(7*x)+500;
  for (int pulseCounter=0; pulseCounter<=50; pulseCounter++)
  {
    mcp.digitalWrite(MOTOR_IN, HIGH);
    delayMicroseconds(del);
    mcp.digitalWrite(MOTOR_IN, LOW);
    delay(20); // Opóźnienie pomiedzy pulsami.
  }
}

/** Funkcja resetująca dane kodu
 *
 *  Funkcja resetuje dane przekazywane do tablic z wartościami kodów wprowadzanych przez użytkownika oraz iterator kodu do wartości początkowych.
 */
void reset_danych()
{
  iterator_kodu=0;
  strncpy(wpisany_kod, "####", sizeof(wpisany_kod));
  strncpy(wpisane_przyciski, "####", sizeof(wpisane_przyciski));
  strncpy(kod_lcd, "    ", sizeof(kod_lcd));
}

/** Funkcja resetująca blokadę zamka.
 *
 *  Funkcja zmienająca pozycję zamką na zamkniętą i resetująca wartości zabezpieczeń przez wywołanie funkcji reset_danych().
 *  Po wywołaniu tej funkcji następuję zapisanie wartości EEPROM z poziomem zamka na 0 (zamknięty) oraz reset poziomu zabezpieczenia na 1.
 */
void reset_zamka()
{
  pozycja_zamka(240);
  EEPROM.write(9, 0);
  ilosc_prob = 3;
  poziom_zabezpieczenia = 1;
  reset_led();
  lcd.clear();
  wypisz_na_lcd(0, 0, "UZBROJONY");
  pozycja_zamka(240);
  delay(1500);
  lcd.clear();
  wypisz_na_lcd(0, 0, "WPROWADZ KOD 1");
  reset_danych();
}

/** Funkcja blokująca zamkek.
 *
 *  W momencie wywołania przez czas 5 sekund niemożliwe jest podjęcie akcji.
 */
void blokada_zamka()
{
  lcd.clear();
  wypisz_na_lcd(0, 0, "BLOKADA.");
  reset_led();
  sterowanie_led(LED_RED, HIGH);
  delay(5000);
  reset_zamka();
}

/** Funkcja resetująca diody LED.
 *  
 *  Kiedy zostaje wywołana wszystkie diody LED gasną.
 */
void reset_led()
{
  sterowanie_led(LED_1,      LOW);
  sterowanie_led(LED_2,      LOW);
  sterowanie_led(LED_3,      LOW);
  sterowanie_led(LED_RED,    LOW);
  sterowanie_led(LED_YELLOW, LOW);
}

/** Funkcja sterująca poszczególnymi diodami LED
 * 
 * @param dioda: Port diody która ma zostać zapalona
 * @param stan: Parametr przesyłający stan w jaki ma wejść dioda. HIGH - zapalona, LOW - zgaszona.
 */
void sterowanie_led(uint8_t dioda, bool stan)
{
  mcp.digitalWrite(dioda, stan);
}

/** Funkcja wypisująca komunikaty na wyświetlaczu LCD.
 *  
 * @param col: Kolumna, w której ma zostać wyświetlony tekst.
 * @param row: Wiersz, w którym ma zostać wyświetlony tekst.
 * @param message: Tablica znaków przechowująca wiadomość do wyświetlenia.
 */
void wypisz_na_lcd(uint8_t col, uint8_t row, const char * message)
{
  lcd.setCursor(col, row);
  lcd.print(message);
}

/** Funkcja weryfikujaca poprawnosc wpisanego kodu 4x4.
 *  
 *  Jeżeli wartość jest poprawna, funkcja zwraca wartość true, w przeciwnym razie, false.
 */
bool czy_kod_1_poprawny()
{
  char key;
  while(true)
  {
    key = read_key();
    if(key != 'E')
    {
      //resetowanie po wciśnięciu C
      if(key == 'C')
      {
        iterator_kodu=0;
        strncpy(wpisany_kod, "####", sizeof(wpisany_kod));
        strncpy(kod_lcd, "    ", sizeof(kod_lcd));
        lcd.clear();
        wypisz_na_lcd(0, 0, "WPROWADZ KOD 1");
        wypisz_na_lcd(0, 1, kod_lcd);
        continue;
      }
      // Mechanizm wprowadzania kodu.
      wpisany_kod[iterator_kodu] = key;
      kod_lcd[iterator_kodu] = '*';
      iterator_kodu=iterator_kodu+1;
      lcd.clear();
      wypisz_na_lcd(0, 0, "WPROWADZ KOD 1");
      wypisz_na_lcd(0, 1, kod_lcd);
      lcd.setCursor(0,1);
      // Sprawdzanie poprawności kodu.
      if(strcmp(wpisany_kod, kod)==0)
      {
        return true;
      }
      if(iterator_kodu==4){
       return false;
      }
    }
  }
}

/** Funkcja sterująca zabezpieczeniem pierwszego poziomu, klawiatury 4x4.

    Zadaniem funkcji jest umożliwienie użytkownikowi wprowadzenia kodu dostępu
    z klawiatury 4x4 z przeanalizowaniem poprawności wprowadzonego kodu za pomocą
    funkcji czy_kod_1_poprawny(). Funkcja pozwala na wprowadzenie niepoprawnego kodu dwukrotnie,
    za trzecim razem następuje blokada zamka poprzez wywołanie funckji blokada_zamka().
    Jeśli kod był prawidłowy, następuje zmiana wartości poziomu zabezpieczenia na 2 i wyjscie z funkcji.
*/
void zabezpieczenie_klawiatury()
{
  lcd.begin(16,2);
  wypisz_na_lcd(0, 0, "WPROWADZ KOD 1");
  while(true)
  {
    if(ilosc_prob==0) // Sprawdzanie dostępnej ilości prób.
    {
      blokada_zamka();
      return;
    }
    
    if(czy_kod_1_poprawny()) // Sprawdzanie poprawności kodu.
    {
      iterator_kodu=0;
      strncpy(wpisany_kod, "####", sizeof(wpisany_kod));
      strncpy(kod_lcd, "    ", sizeof(kod_lcd));
      //Zapalenie diody
      sterowanie_led(LED_1, HIGH);
      lcd.clear();
      wypisz_na_lcd(0, 0, "KOD PRAWIDLOWY");
      delay(2000);
      lcd.clear();
      wypisz_na_lcd(0, 0, "WPROWADZ KOD 2");
      poziom_zabezpieczenia = 2;
      ilosc_prob=3;
      return;
    }
    else // Resetowanie kodu i iteratora po przekroczeniu wartosci.
    {      
      iterator_kodu=0;
      strncpy(wpisany_kod, "####", sizeof(wpisany_kod));
      strncpy(kod_lcd, "    ", sizeof(kod_lcd));
      ilosc_prob=ilosc_prob-1;
      lcd.clear();
      wypisz_na_lcd(0, 0, "BLEDNY KOD!");
      wypisz_na_lcd(0, 1, "PROBY: ");
      char ilosc_prob_c[10];
      sprintf(ilosc_prob_c, "%d", ilosc_prob); //cast do typu char dla LCD
      wypisz_na_lcd(7, 1, ilosc_prob_c);
      delay(1500);
      lcd.clear();
      wypisz_na_lcd(0, 0, "WPROWADZ KOD 1");
    }
  }
}

/** Funkcja sprawdzająca poprawność wpisania kodu dla przycisków analogowych.

    Jeżeli wartość jest poprawna, funkcja zwraca wartość true, w przeciwnym razie, false.
*/
bool czy_kod_2_poprawny()
{
  while(true)
  {
    wpisane_przyciski[iterator_kodu]=read_button();
    kod_lcd[iterator_kodu] = '*';
    iterator_kodu=iterator_kodu+1;
    lcd.clear();
    wypisz_na_lcd(0, 0, "WPROWADZ KOD 2");
    wypisz_na_lcd(0, 1, kod_lcd);
    
    if(iterator_kodu==4) // Sprawdź czy zostały wprowadzone 4 znaki.
    {
      if(strcmp(wpisane_przyciski, kolejnosc_przyciskow)==0) // Jeśli wpisany kod jest poprawny zwróć wartoć true.
      {
        return true;
      }
      else // W przeciwnym razie zwróć false.
      {
      return false;
      }
    }
  }
}

/** Funkcja sterująca zabezpieczeniem drugiego poziomu, przycisków analogowych.

    Zadaniem funkcji jest umożliwienie użytkownikowi wprowadzenia kodu dostępu
    za pomocą przycisków analogowych z przeanalizowaniem poprawności wprowadzonego kodu za pomocą
    funkcji czy_kod_2_poprawny(). Funkcja pozwala na wprowadzenie niepoprawnego kodu dwukrotnie,
    za trzecim razem następuje blokada zamka poprzez wywołanie funckji blokada_zamka().
    Jeśli kod był prawidłowy, następuje zmiana wartości poziomu zabezpieczenia na 3 i wyjscie z funkcji.
*/
void zabezpieczenie_przyciskow()
{
  while(true)
  {
    if(ilosc_prob==0)
    {
      blokada_zamka();
      return;
    }
    // Sprawdzanie poprawności kodu.
    if(czy_kod_2_poprawny() == true){
      iterator_kodu=0;
      strncpy(wpisane_przyciski, "####", sizeof(wpisane_przyciski));
      strncpy(kod_lcd, "    ", sizeof(kod_lcd));
      sterowanie_led(LED_2, HIGH);
      lcd.clear();
      wypisz_na_lcd(0, 0, "KOD PRAWIDLOWY");
      delay(2000);
      lcd.clear();
      wypisz_na_lcd(0, 0, "WPROWADZ KARTE");
      ilosc_prob=3;
      poziom_zabezpieczenia = 3;
      return;
    } else {
      iterator_kodu=0;
      strncpy(wpisane_przyciski, "####", sizeof(wpisane_przyciski));
      strncpy(kod_lcd, "    ", sizeof(kod_lcd));
      ilosc_prob=ilosc_prob-1;
      lcd.clear();
      wypisz_na_lcd(0, 0, "BLEDNY KOD!");
      wypisz_na_lcd(0, 1, "PROBY: ");
      char ilosc_prob_c[10];
      sprintf(ilosc_prob_c, "%d", ilosc_prob); //cast do typu char dla LCD
      wypisz_na_lcd(7, 1, ilosc_prob_c);
      delay(1500);
      lcd.clear();
      wypisz_na_lcd(0, 0, "WPROWADZ KOD 2");
    }
  }
}

/** Funkcja sterująca zabezpieczeniem trzeciego poziomu, czytnika kart RFID.
 *  
 *  Funkcja korzysta z elementów biblioteki MCP_MFRC522. Kod jest bezpośrednio zapożyczony z przykładów zawartych w plikach biblioteki
 *  i odpowiednio zmieniony na potrzeby projektu.
 *  Funkcja pozwala na wprowadzenie złej karty dwukrotnie, za trzecim razem blokując dostęp do zamka za pomocą funkcji blokada_zamka().
 */
void zabezpieczenie_rfid()
{
  while(true)
  {
    if(ilosc_prob==0)
    {
      blokada_zamka();
      return;
    }
    // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
    if (!rfid.PICC_IsNewCardPresent())
      return;
    
    // Verify if the NUID has been readed
    if (!rfid.PICC_ReadCardSerial())
    {
      return;
    }
    
    MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
    
    // Check is the PICC of Classic MIFARE type
    if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&  
        piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
        piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
      return;
    }
  
    if (rfid.uid.uidByte[0] == nuidCODE[0] || 
        rfid.uid.uidByte[1] == nuidCODE[1] || 
        rfid.uid.uidByte[2] == nuidCODE[2] || 
        rfid.uid.uidByte[3] == nuidCODE[3] ) {
      sterowanie_led(LED_3, HIGH);
      lcd.clear();
      wypisz_na_lcd(0, 0, "UZYSKANO DOSTEP");
      poziom_zabezpieczenia=4;
      return;
    } else {
      ilosc_prob=ilosc_prob-1;
      lcd.clear();
      wypisz_na_lcd(0, 0, "ZLA KARTA!");
      wypisz_na_lcd(0, 1, "PROBY: ");
      char ilosc_prob_c[10];
      sprintf(ilosc_prob_c, "%d", ilosc_prob); //cast do typu char dla LCD
      wypisz_na_lcd(7, 1, ilosc_prob_c);
      delay(1500);
      lcd.clear();
      wypisz_na_lcd(0, 0, "WPROWADZ KARTE");
    }
  }
}

/** Funkcja zawierajaca instrukcje stanu serwisowego.
 *  
 *  W stanie serwisowym użytkownik może zmienić kody dostępu dla klawiatury 4x4 oraz przycisków analogowych.
 *  Użytkownik jest odpytywany przez użądzenie który kod zmienić i zapisuje nowe kody do pamięci EEPROM.
 */
void stan_serwisowy()
{
  reset_danych();
  reset_led();
  sterowanie_led(LED_YELLOW, HIGH);
  lcd.clear();
  wypisz_na_lcd(0, 0, "ZMIANA KODU 1");
  wypisz_na_lcd(0, 1, "'A' OK 'C' POMIN");
  char key;
  bool zmien_kod = true;
  // Zmiana kodu klawiatury 4x4.
  while(zmien_kod)
  {
    key = read_key();
    if(key != 'E')
    {
      if(key == 'A')
      {
        lcd.clear();
        wypisz_na_lcd(0, 0, "ZMIANA KODU 1");
        wypisz_na_lcd(0, 1, "WPROWADZ OBECNY KOD 1");
        delay(1500);
        lcd.clear();
        wypisz_na_lcd(0, 0, "WPROWADZ KOD 1");
        bool czy_kod_poprawny = czy_kod_1_poprawny();
        if(czy_kod_poprawny == true)
        {
          reset_danych();
          lcd.clear();
          wypisz_na_lcd(0, 0, "PODAJ NOWY KOD 1");
          char key;
          bool zmien_1 = true;
          while(true)
          {
            key = read_key();
            if(key != 'E' && key != 'C')
            {
              // Mechanizm wpisywania kodu.
              wpisany_kod[iterator_kodu] = key;
              kod_lcd[iterator_kodu] = '*';
              iterator_kodu=iterator_kodu+1;
              lcd.clear();
              wypisz_na_lcd(0, 0, "PODAJ NOWY KOD 1");
              wypisz_na_lcd(0, 1, kod_lcd);
              lcd.setCursor(0,1);
              // Sprawdzanie kodu.
              if(iterator_kodu == 4)
              {
                strncpy(kod, wpisany_kod, sizeof(kod));
                for(byte i=0; i<4; i++)
                {
                  EEPROM.write(i, kod[i]-48); // Wartość 48 z tablicy ASCII dla cyfr.
                }
                lcd.clear();
                wypisz_na_lcd(0, 0, "KOD 1 ZMIENIONY.");
                delay(1500);
                zmien_kod = false;
                break;
              }
            }
          }
        } else {
          blokada_zamka();
        }
      }
      if(key == 'C')
      {
        zmien_kod = false;
      }
    }
  }
  lcd.clear();
  wypisz_na_lcd(0, 0, "ZMIANA KODU 2");
  wypisz_na_lcd(0, 1, "'A' OK 'C' POMIN");
  zmien_kod = true;
  reset_danych();
  // Zmiana kodu przycisków analogowych.
  while(zmien_kod)
  {
    key = read_key();
    if(key != 'E')
    {
      if(key == 'A')
      {
        lcd.clear();
        wypisz_na_lcd(0, 0, "ZMIANA KODU 2");
        wypisz_na_lcd(0, 1, "WPROWADZ OBECNY KOD 2");
        delay(1500);
        lcd.clear();
        wypisz_na_lcd(0, 0, "WPROWADZ KOD 2");
        if(czy_kod_2_poprawny())
        {
          lcd.clear();
          wypisz_na_lcd(0, 0, "PODAJ NOWY KOD 2");
          reset_danych();
          while(true)
          {
            wpisane_przyciski[iterator_kodu]=read_button();
            kod_lcd[iterator_kodu] = '*';
            iterator_kodu=iterator_kodu+1;
            lcd.clear();
            wypisz_na_lcd(0, 0, "WPROWADZ KOD 2");
            wypisz_na_lcd(0, 1, kod_lcd);

            if(iterator_kodu==4)
            {
              strncpy(kolejnosc_przyciskow, wpisane_przyciski, sizeof(kolejnosc_przyciskow));
              for(byte i=0; i<4; i++)
              {
                EEPROM.write(i+4, kolejnosc_przyciskow[i]-48); // Wartość 48 z tablicy ASCII dla cyfr.
              }
              lcd.clear();
              wypisz_na_lcd(0, 0, "KOD 2 ZMIENIONY.");
              delay(1500);
              zmien_kod = false;
              break;
            }
          }
        } else {
          blokada_zamka();
          return;
        }
      }
      if(key == 'C')
      {
        zmien_kod = false;
      }
    }
  }
  reset_danych();
  reset_led();
  lcd.clear();
  wypisz_na_lcd(0, 0, "* - uzbrojenie");
  wypisz_na_lcd(0, 1, "# - serwis");
  return;
}

/** Funkcja sterująca zachowaniem zamka po jego otwarciu.
 *  
 *  Funkcja pozwala połączyć się z zamkiem przez telefon za pomocą wi-fi w celu uzbrojenia zamka,
 *  umożliwia również przejście do trybu serwisowego za pomocą przycisku '#' na klawiaturze 4x4
 *  i następnie wywołania funckji stan_serwioswy(), lub uzbrojenia zamka za pomocą przycisku '*'.
 */
void otwarty_zamek()
{
  pozycja_zamka(0);
  EEPROM.write(9, 1);
  lcd.clear();
  wypisz_na_lcd(0, 0, "* - uzbrojenie");
  wypisz_na_lcd(0, 1, "# - serwis");
  char key;
  while(true)
  {
    RemoteXY_Handler();
    if(RemoteXY.button_1==1)
    {
      RemoteXY.button_1=0;
      reset_zamka();
      return;
    }
    key = read_key();
    if(key != 'E')
    {
      // uzbrojenie zamka po wcisnieciu *
      if(key == '*')
      {
        reset_zamka();
        return;
      }
      // przejscie w stan serwisowy po wcisnieciu #
      if(key == '#')
      {
        stan_serwisowy();
        return;
      }
    }
  }
}

/** Główna pętla programu.
 *  
 *  Przy pierwszej iteracji sprawdza zapis kodów w pamięci EEPROM.
 *  Jeśli kody nigdy nie zostały zapisane do pamięci, zezwoli na wprowadzenie kodów  
 *  poprzez wywołanie funckji wprowadz_kody().
 *  W następnym kroku funkcja steruje poziomami zebezpieczień za pomocą instrukcji if/else if
 *  i wywoływaniem odpowiednich funkcji sterujących.
 */
void loop()
{
  if(read_eeprom)
  {
    for(byte i=0; i<4; i++)
    {
      ep_keypad[i] = EEPROM.read(i);
      ep_buttons[i] = EEPROM.read(i+4);
      if(ep_keypad[i]==255 || ep_buttons[i]==255)
      {
        wprowadz_kody();
        break;
      }
      kod[i]=ep_keypad[i]+48; // Wartość 48 z tablicy ASCII dla cyfr.
      kolejnosc_przyciskow[i]=ep_buttons[i]+48; // Wartość 48 z tablicy ASCII dla cyfr.
    }
    read_eeprom=false;
  }
  
  if(poziom_zabezpieczenia == 1)
  {
    zabezpieczenie_klawiatury();
  } 
  else if(poziom_zabezpieczenia == 2)
  {
    zabezpieczenie_przyciskow();
  } 
  else if(poziom_zabezpieczenia == 3) 
  {
    zabezpieczenie_rfid();
  } 
  else if(poziom_zabezpieczenia == 4) 
  {
    otwarty_zamek();
  }
}